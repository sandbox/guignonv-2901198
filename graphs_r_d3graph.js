/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_r_d3graph = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  render: function (graph_index, rdr_index) {
    var renderer = Drupal.settings.graphs.renderers[rdr_index];
    var graph = Drupal.settings.graphs.graphs[graph_index];
    var sourcer = Drupal.settings.graphs.sourcers[graph.sourcer];
    // Get graph.
    var graph_data;
    Drupal.behaviors.graphs.getGraph(
      graph_index,
      graph.sourcer,
      {
        graph_id: graph.id,
        node_id: (renderer.root && renderer.root.trim() ? renderer.root : null),
        depth: (renderer.default_depth ? renderer.default_depth : 1),
        rel_types: (renderer.parent_of_relationships ? renderer.parent_of_relationships : ['parent_of']),
        back_rel_types: (renderer.child_of_relationships ? renderer.child_of_relationships : ['child_of']),
        callback: function (sourcer_data) {graph_data = sourcer_data;}
      }
    );

    // Display graph title.
    if (renderer.graph_title) {
      $('#graphs_' + graph_index)
        .prepend("<h2>" + renderer.graph_title + "</h2>\n");
    }

    // Create SVG element.
    $('#graphs_' + graph_index)
      .append(
        '<svg class="graphs_r_d3graph" width="'
        + (graph.width ? graph.width : '100%')
        + '" height="'
        + (graph.height ? graph.height : '500')
        + '"></svg>');
    var svg = d3.select('#graphs_' + graph_index + " svg");

    // Drawing area padding.
    // @todo: replace fixed value by a dynamic one computed on the root label.
    var display_margin = 50;
    // Size of the rendering area.
    var viewer_width = +svg.attr("width");
    var viewer_height = +svg.attr("height");
    var duration = 750;

    // Base SVG group element.
    var g = svg.append("g");
    var g_links = g.append("g");
    var g_nodes = g.append("g");

    // Arrows.
    svg.append("defs").selectAll("marker")
        .data(["end-arrow", "licensing", "resolved"])
      .enter().append("marker")
        .attr("id", function(d) { return d; })
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 18)
        .attr("refY", 0)
        .attr("markerWidth", 6)
        .attr("markerHeight", 6)
        .attr("orient", "auto")
      .append("path")
        .attr("d", "M0,-5L10,0L0,5 L10,0 L0, -5")
        .style("stroke", "#555");

    // Zoom management.
    var zoom_listener = d3.zoom()
      .scaleExtent([(renderer.zoom_enabled ? 0.25 : 1), (renderer.zoom_enabled ? 50 : 1)])
      .translateExtent(
        renderer.pan_enabled ?
        [[-200, -200], [viewer_width + 200, viewer_height + 200]]
        : [[0, 0], [viewer_width, viewer_height]]
      )
      .on("zoom", zoomHandler);

    // Force layout.
    var simulation = d3.forceSimulation()
      .force("link", d3.forceLink().id(function(d) { return d.id; }))
      .force("charge", d3.forceManyBody())
      .force("center", d3.forceCenter(viewer_width / 2, viewer_height / 2));

    /**
     * Loads additional child nodes at a given position.
     */
    function loadNodes(d) {
      // Get linked nodes.
      Drupal.behaviors.graphs.getGraph(
        graph_index,
        graph.sourcer,
        {
          graph_id: graph.id,
          node_id: d.id,
          depth: (renderer.default_depth ? renderer.default_depth : 1),
          callback: updateSubgraph
        }
      );
    }


    /**
     * Handles click action on a node.
     */
    function clickNode(d) {
      if (d3.event.defaultPrevented) return;
      
      if (!d.link_loaded) {
        loadNodes(d);
        d.link_loaded = true;
      }
    }

    /**
     * Handles zooming and panning.
     */
    function zoomHandler() {
      if (!renderer.pan_enabled) {
        d3.event.transform.x = 0;
        d3.event.transform.y = 0;
      }
      g.attr("transform", d3.event.transform);
      // @todo: find a way to prevent scrolling when zoom reached its limits.
      // if (1 >= d3.event.transform.k) {
      //   d3.event.preventDefault();
      //   d3.event.stopPropagation();
      //   d3.event.stopImmediatePropagation();
      // }
    }

    /**
     * Reset graph display.
     */
    function resetgraph() {
      svg.transition()
          .duration(duration)
          .call(zoom_listener.transform, d3.zoomIdentity);
    }

    /**
     *
     */
    // @todo: add zoom in/out/reset buttons.
    // @todo: add blury border to graph when zoomed.
    // @todo: add functions for sorting nodes.

    function dragstarted(d) {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

    /**
     * Draw or re-draw a subgraph at a given node.
     *
     * This is the main graph rendering function.
     */
    function updateSubgraph() {

      var link = g_links.selectAll("g.link")
        .data(graph_data.links);
      
      var link_enter = link.enter().append('g')
        .attr("class", "link");

      var links = link_enter.append("line")
          .attr("stroke-width", 1)
          .style("marker-end",  "url(#end-arrow)");

      var node = g_nodes.selectAll('g.node')
        .data(graph_data.nodes);
        
      var node_enter = node.enter().append('g')
        .attr('class', 'node')
        .call(d3.drag()
          .on('start', dragstarted)
          .on('drag', dragged)
          .on('end', dragended)
        );

      var nodes = node_enter.append('circle')
        .attr('cx', function (d) { return d.x;})
        .attr('cy', function (d) { return d.y;})
        .attr('r', 5)
        .on('click', clickNode);

      // Tooltips.
      node_enter.append('title')
        .text(function(d) { return d.definition; }); //+FIXME: definition --> tooltip

      // Label.
      var labels = node_enter.append('text')
        .attr('class', 'label')
        .attr('dx', 0)
        .attr('dy', -6)
        .text(function(d) { return d.name; })
        .style('text-anchor', 'middle');

      function ticked() {
        // Links.
        g_links.selectAll('g.link line')
          .attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });
      
        // Nodes.
        g_nodes.selectAll('g.node circle')
          .attr("cx", function(d) { return d.x; })
          .attr("cy", function(d) { return d.y; });

        g_nodes.selectAll('g.node .label')
          .attr("x", function(d) { return d.x; })
          .attr("y", function(d) { return d.y; });
      }
      
      simulation
        .nodes(graph_data.nodes)
        .on('tick', ticked);
      
      simulation.force('link')
        .links(graph_data.links);

    }

    // Manages zooming and panning events.
    svg.call(zoom_listener);
    // Renders graph.
    updateSubgraph();

  }
};

}(jQuery));
